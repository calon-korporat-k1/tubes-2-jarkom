# Tubes 2 Jarkom

## Nama Kelompok
Sabai

## Petunjuk Penggunaan Program
1. Jalankan MainServer.py di command prompt dengan perintah `py MainServer.py`
2. Buka command prompt baru dan jalankan ngrok.exe dengan perintah `ngrok`
3. Masukkan *authorization token* ngrok dengan perintah `ngrok authtoken <insert auth token here>`
4. Jalankan perintah `ngrok http <port yang digunakan untuk menjalankan MainServer.py>`
5. *Copy* URL Forwarding pada ngrok ke Submission Form dan ganti `http` menjadi `ws`, lalu klik tombol Submit

## Pembagian Tugas
| NIM | Nama | Apa yang dikerjakan | Persentase kontribusi |
| --- | --- | --- | --- |
| 13517001 | Farhan Ramadhan Syah Khair | Control frame, !echo | 35% |
| 13517049 | Gama Pradipta Wirawan | !submission, checksum | 35% |
| 13517055 | Ahmad Naufal Hakim | Handshake, Framing | 30% |
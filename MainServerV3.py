import socket, hashlib, base64, _thread, errno, struct
import logging
import hashlib
logger = logging.getLogger(__name__)
logging.basicConfig()

# All Variable
ConnectionString = "Connection: Upgrade"
UpgradeString = "Upgrade: websocket"
SecretWebSocketKeyString = "Sec-WebSocket-Key"
WSSKey = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
HOST,PORT = '127.0.0.1', 8080
FIN    = 0x80
OPCODE = 0x0f
MASKED = 0x80
PAYLOAD_LEN = 0x7f
PAYLOAD_LEN_EXT16 = 0x7e
PAYLOAD_LEN_EXT64 = 0x7f
OPCODE_CONTINUATION = 0x0
OPCODE_TEXT         = 0x1
OPCODE_BINARY       = 0x2
OPCODE_CLOSE_CONN   = 0x8
OPCODE_PING         = 0x9
OPCODE_PONG         = 0xA

def encode_to_UTF8(data):
    try:
        return data.encode('UTF-8')
    except UnicodeEncodeError as e:
        logger.error("Could not encode data to UTF-8 -- %s" % e)
        return False
    except Exception as e:
        raise(e)
        return False

def try_decode_UTF8(data):
    try:
        return data.decode('utf-8')
    except UnicodeDecodeError:
        return False
    except Exception as e:
        raise(e)

# def read_bytes(sock,num):
#     bytes = sock.recv(num)
#     return bytes

# def readAll(sock):
#     BUFF_SIZE = 2**16
#     data = b''
#     while True:
#         part = sock.recv(BUFF_SIZE)
#         data += part
#         if len(part) < BUFF_SIZE:
#             break
#     return data

def handshake(req):
    # self.request is the TCP socket connected to client
    data = req.decode().strip()
    headers = data.split("\r\n")

    # To Check Whether Websocket Request Or Not
    if ConnectionString in data and UpgradeString in data:
        for header in headers:
            if SecretWebSocketKeyString in header:
                key = header.split(" ")[1]
        return handshakeHandle(key)
    else:
        return "HTTP/1.1 400 Bad Request\r\n" + \
                                "Content-Type: text/plain\r\n" + \
                                "Connection: close\r\n" + \
                                "\r\n" + \
                                "Incorrect request"

def handshakeHandle(key):
    key = key + WSSKey
    respondKey = base64.standard_b64encode(hashlib.sha1(key.encode()).digest()).strip()

    respond="HTTP/1.1 101 Switching Protocols\r\n" + \
            "Upgrade: websocket\r\n" + \
            "Connection: Upgrade\r\n" + \
            "Sec-WebSocket-Accept: %s\r\n\r\n"% respondKey.decode("ASCII")

    return respond.encode()

def handshake1(req):
    headers = req.decode().strip().split('\r\n')
    if "Connection: Upgrade" not in headers or "Upgrade: websocket" not in headers:
        return False
    key = ''
    for h in headers:
        if "Sec-WebSocket-Key" in h:
            key = h.split(" ")[1]
            break
    
    GUID = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11'
    hash = hashlib.sha1(key.encode() + GUID.encode())
    resp_key = base64.standard_b64encode(hash.digest()).strip()
    resp =  'HTTP/1.1 101 Switching Protocols\r\n'\
            'Upgrade: websocket\r\n'              \
            'Connection: Upgrade\r\n'             \
            'Sec-WebSocket-Accept: %s\r\n\r\n' % resp_key.decode("ASCII")
    return resp.encode()

def decode_frame(conn,frame):
    fin = frame[0] & FIN
    opcode = frame[0] & OPCODE
    masked = frame[1] & MASKED
    payload_len = frame[1] & PAYLOAD_LEN
    payload_start = 6
    mask = frame[2:6]
    if payload_len == 126:
        payload_len = struct.unpack(">H", frame[2:4])[0]
        mask = frame[4:8]
        payload_start = 8
    
    elif payload_len == 127:
        payload_len = struct.unpack(">Q",frame[2:10])[0]
        mask = frame[10:14]
        payload_start = 14
    message_bytes = bytearray()

    for message_byte in frame[payload_start:payload_start+payload_len]:
        message_byte ^= mask[len(message_bytes)%4]
        message_bytes.append(message_byte)

    return opcode,masked,message_bytes

# def handle_text(conn,payload):
#     command, msg = payload.split(' ',1)
#     if (command == "!echo"):
#         return send_frame(conn,msg)
#     elif (command == "!submission"):
#         file = open("MainServer.zip","rb")
#         msg = file.read()
#         return send_frame(conn,msg)
#     else :
#         return False

# def handle_binary(conn, payload):
#     command, msg = payload.split(' ',1)
#     if hashlib.md5(msg).hexdigest() == hashlib.md5(read_file(open("MainServer.zip", 'rb'))).hexdigest():
#         send_frame(conn,"1")
#     else:
#         send_frame(conn,"0")

def checksum(payload):
    file = open("MainServerV3.zip","rb")
    file_r = file.read()
    md5_file = hashlib.md5(file_r).hexdigest()
    md5_payload = hashlib.md5(payload).hexdigest()
    if md5_file.lower() == md5_payload.lower():
        return bytes("1",'utf8')
    else:
        return bytes("0",'utf8')

def handle_message(conn, opcode,payload):
    if opcode == OPCODE_PING:
        handle_ping(conn,payload)
    elif opcode == OPCODE_PONG:
        pass
    elif opcode == OPCODE_CONTINUATION:
        send_frame(conn,payload,opcode)
    else:
        try:
            command = payload.decode('utf-8').split(' ',1)
            if command[0] == "!echo":
                send_frame(conn,command[1].encode('utf8'),OPCODE_TEXT)
            elif command[0] == "!submission":
                print("Read file")
                file = open("MainServerV3.zip","rb")
                send_frame(conn,file.read(),OPCODE_BINARY)
            else:
                print("binary")
                send_frame(conn,checksum(payload),OPCODE_TEXT)
        except UnicodeDecodeError:
            print("binary")
            send_frame(conn,checksum(payload),OPCODE_TEXT)

def read_file(file):
    with file:
        return file.read()

def handle_ping(conn,payload):
    send_frame(conn,payload,OPCODE_PONG)

def handle_pong(conn,payload):
    pass

def send_frame(conn,payload,opcode):
    print("Send payload process " , payload)
    header = bytearray()
    payload_len = len(payload)
    if payload_len <=125:
        print("length 125")
        header.append(FIN | opcode)
        header.append(payload_len)
    elif payload_len >= 126 and payload_len <=65535:
        print("length 126")
        header.append(FIN | opcode)
        header.append(PAYLOAD_LEN_EXT16)
        header.extend(struct.pack(">H",payload_len))
    else:
        print("length more")
        header.append(FIN | opcode)
        header.append(PAYLOAD_LEN_EXT64)
        header.extend(struct.pack(">Q",payload_len))
    print("going to send")
    conn.send(header + payload)
    print ("sent ;)")

def new_client(conn, address):
    while True:
        req = conn.recv(2**16).strip()
        conn.send(handshake(req))
        print("Connection : ", address[0],':',address[1])
        while True:
            frame = conn.recv(2**16).strip()
            opcode , masked, payload = decode_frame(conn,bytearray(frame.strip()))
            
            if (opcode == OPCODE_CLOSE_CONN):
                break
            else:
                print(opcode, payload)
                handle_message(conn,opcode,payload)
                print("Send : ", payload ," to : ",address[0],':',address[1])
        conn.close()
        print("Connection Closed : ", address[0],':',address[1])
        break
    conn.close()

def main():
    webSocket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    webSocket.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    webSocket.bind((HOST,PORT))
    webSocket.listen(10)

    while True:
        connection, address = webSocket.accept()
        _thread.start_new_thread(new_client,(connection,address))
        
if __name__ == "__main__":
    main()